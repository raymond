#version 330 compatibility

/*
Raymond - a physics-inspired ray tracer for Fragmentarium
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

#include "Raymond-sRGB.frag"
#include "Raymond.frag"

#group Mandelbrot
// maximum period for atoms and domains
uniform int Period; slider[1,1,128]
// minimum size of atoms and domains
uniform float MinSize; slider[-24,-10,1];
// maximum iteration count for exterior
uniform int Iterations; slider[1,1,1000];
// thickness of exterior fringe
uniform float Thickness; slider[-24,-10,1];

// standard preamble

uint hash(uint a)
{
  return hash_burtle_9(a);
}

Ray camera(Random PRNG, Camera C, vec2 coord, out float intensity)
{
  return pinhole(PRNG, C, -coord, pinhole_uniforms(), intensity);
}

float raytrace(Random PRNG, Ray V, out Hit h)
{
  return raytrace_de(PRNG, V, raytrace_de_uniforms(), h);
}

vec3 film(Random PRNG, float wavelength, float intensity) {
  return xyz2rgb(observer(wavelength) * intensity);
}

float screen(vec4 tex, float wavelength)
{
  return CRT(sRGB2linear(tex.rgb), wavelength);
}

vec4 light(Random PRNG, vec3 from, vec3 dir)
{
  return vec4(0.0);
}

// Mandelbrot numerics

// https://mathr.co.uk/blog/2018-11-17_newtons_method_for_periodic_points.html
vec2 Mandelbrot_nucleus(vec2 c0, int period)
{
  vec2 c = c0;
  for (int j = 0; j < 16; ++j)
  {
    vec4 G = cConst(1.0);
    vec4 z = cConst(0.0);
    c0 = c;
    for (int l = 1; l <= period; ++l)
    {
      z = cSqr(z) + cVar(c);
      if (l < period && period % l == 0)
        G = cMul(z, G);
    }
    G = cDiv(z, G);
    c = c0 - cDiv(G.xy, G.zw);
    if (! (c != c0)) break;
  }
  return c;
}

// https://mathr.co.uk/blog/2016-12-24_deriving_the_size_estimate.html
vec2 Mandelbrot_size(vec2 c, int period)
{
  vec2 l = vec2(1.0, 0.0);
  vec2 b = vec2(1.0, 0.0);
  vec2 z = vec2(0.0, 0.0);
  for (int i = 1; i < period; ++i)
  {
    z = cSqr(z) + c;
    l = 2.0 * cMul(z, l);
    b = b + cDiv(vec2(1.0, 0.0), l);
  }
  return cDiv(vec2(1.0, 0.0), cMul(b, cSqr(l)));
}

// https://mathr.co.uk/blog/2013-04-01_interior_coordinates_in_the_mandelbrot_set.html
// https://mathr.co.uk/blog/2015-01-26_newtons_method_for_misiurewicz_points.html
vec4 Mandelbrot_attractor(vec2 c, vec2 z_initial, int period)
{
  vec4 z = cVar(z_initial);
  vec2 dz = vec2(0.0);
  for (int j = 0; j < 16; ++j)
  {
    vec4 z0 = z;
    vec4 G = cConst(1.0);
    for (int l = 1; l <= period; ++l)
    {
      z = cSqr(z) + cConst(c);
      if (l < period && period % l == 0)
        G = cMul(z - z0, G);
    }
    dz = z.zw;
    G = cDiv(z - z0, G);
    z = cVar(z0.xy - cDiv(G.xy, G.zw));
    if (! (z != z0)) break;
  }
  return vec4(z.xy, dz);
}

// trace ray from a nucleus, more stable than trying to do it one jump
vec4 Mandelbrot_attractor_ray(vec2 nucleus, vec2 target, int period)
{
  const int steps = 16;
  vec4 P = vec4(0.0);
  for (int ray = 1; ray <= steps; ++ray)
  {
    float t = float(ray) / float(steps);
    P = Mandelbrot_attractor(mix(nucleus, target, t), P.xy, period);
  }
  return P;
}

// https://mathr.co.uk/blog/2013-12-10_atom_domain_size_estimation.html
float Mandelbrot_domain_size(vec2 c, int p)
{
  vec4 z = cVar(c);
  float abszq = length(z.xy);
  for (int q = 2; q <= p; ++q) {
    z = cSqr(z) + cVar(c);
    float abszp = length(z.xy);
    if (abszp < abszq && q < p) {
      abszq = abszp;
    }
  }
  return abszq / length(z.zw);
}

// main DE function

vec3 Mandelbrot(vec3 object, float bg)
{
  // FIXME should be able to take uniforms as arguments
  int maxPeriod = Period;
  float minSize = pow(2.0, MinSize);
  int maxIters = Iterations;
  float thickness = pow(2.0, Thickness);
  // no transformations supported
  vec2 c = object.xy;
  vec2 z = vec2(0.0, 0.0);
  // derivative for exterior distance
  vec2 dc = vec2(0.0, 0.0);
  // minimums for atom domains
  vec2 mz = vec2(0.0, 0.0);
  float mr2 = 1.0 / 0.0;
  // calculate 3x (DE,material,bubblethicknessfactor) for CSG operations
  vec3 back = vec3(bg, 0.0, 0.0);
  if (bg < 0.0)
    return back;
  vec3 far = vec3(1.0/0.0, 1.0, 0.0);
  vec3 de_nucleus  = far;
  vec3 de_domain   = far;
  vec3 de_exterior = far;
  int escaped = 0;
  int period = 1;
  for (; period <= maxPeriod; ++period)
  {
    dc = 2.0 * cMul(z, dc) + vec2(1.0, 0.0);
    z = cSqr(z) + c;
    float r2 = dot(z, z);
    if (r2 < 1.5 * mr2)
    {
      vec2 nucleus = Mandelbrot_nucleus(c, period);
      if (period > 1)
      {
        vec2 atom = cDiv(z, mz);
        float S = Mandelbrot_domain_size(nucleus, period);
        if (S > minSize)
        {
          // make a DE-traceable shell out of two surfaces inside each other
          float a = length(vec3(atom, object.z / S));
          vec3 d1 = vec3( (a - 1.01) * S, 3.0, clamp(object.z / S + 1.0, 0.01, 1.99));
          vec3 d2 = vec3(-(a - 0.99) * S, 4.0, 0.0);
          float d = max(d1.x, d2.x);
          if (d < de_domain.x) de_domain = d1.x > d2.x ? d1 : d2;
        }
      }
      if (true)
      {
        vec2 atom = Mandelbrot_attractor_ray(nucleus, c, period).zw;
        float S = 0.5 * length(Mandelbrot_size(nucleus, period));
        if (S > minSize)
        {
          float d = (length(vec3(atom, object.z / S)) - 1.0) * S;
          if (d < de_nucleus.x) de_nucleus = vec3(d, 2.0, 0.0);
        }
      }
      if (r2 < mr2)
      {
        mz = z;
        mr2 = r2;
      }
      if (de_nucleus.x < 0.0)
      {
        escaped = -1;
        break;
      }
    }
    if (r2 > 65536.0)
    {
      escaped = 1;
      break;
    }
  }
  if (escaped == 0)
  {
    for (; period <= maxIters; ++period)
    {
      dc = 2.0 * cMul(z, dc) + vec2(1.0, 0.0);
      z = cSqr(z) + c;
      float r2 = dot(z, z);
      if (r2 > 65536.0)
      {
        escaped = 1;
        break;
      }
    }
  }
  if (escaped == 1)
  {
    float d = length(z) * log(length(z)) / length(dc);
    d = length(vec2(d, object.z)) - thickness;
    if (d < de_exterior.x) de_exterior = vec3(d, 1.0, 0.0);
  }
  // disjoint union(b, a - b) = union(b, intersection(a, invert(b)))
  // this hopefully fixes unsightly issues at atom/domain intersections
  if (isnan(de_exterior.x)) de_exterior = back;
  if (isnan(de_nucleus.x)) de_nucleus = back;
  if (isnan(de_domain.x)) de_domain = back;
  vec3 solid  = de_exterior.x < de_nucleus.x ? de_exterior : de_nucleus;
  vec3 bubble = -solid.x > de_domain.x ? vec3(-solid.x, solid.y, solid.z) : de_domain;
  vec3 mandel = solid.x < bubble.x ? solid : bubble;
  vec3 result = mandel.x < back.x ? mandel : back;
  return result;
}

// delta normal calculation

#define DELTANORMAL(DE,E,p) normalize(vec3 \
  ( DE(p + vec3(E,0.0,0.0)) - DE(p - vec3(E,0.0,0.0)) \
  , DE(p + vec3(0.0,E,0.0)) - DE(p - vec3(0.0,E,0.0)) \
  , DE(p + vec3(0.0,0.0,E)) - DE(p - vec3(0.0,0.0,E)) \
  ))

// main entry points

Hit scene(Scene_HIT tag, Random PRNG, Ray V)
{
//  return Union(Light(Sphere(tag, Identity(), V), V, 1.0), SkyBox(tag, Identity(), V));
  Scene_DE DE;
  vec3 object = V.origin;
  Transform T = Identity();
  float bg = SkyBox(DE, T, V);
  vec3 de = Mandelbrot(object, bg);
  if (! (de.y > 0.0)) return SkyBox(tag, T, V);
  const float epsilon = 1.0e-3; // FIXME
#define M(p) Mandelbrot(p, SkyBox(DE, T, Ray((p), V.direction, V.wavelength, V.index))).x
  vec3 normal = DELTANORMAL(M, epsilon, object);
  // surface
  Surface S;
  S.position = object;
  S.normal = normal;
  S.de = de.x;
  vec2 water = Water_nk(V.wavelength);
  // FIXME the magic numbers to chose materials from DE are annoyment
  if (de.y == 1.0) return Light(S, V, 10.0);
  if (de.y == 2.0) return Glass(PRNG, S, V);
  if (de.y == 3.0) return SoapBubble(PRNG, S, V, 1000.0 * de.z, water);
  if (de.y == 4.0) return Transmit(S, V);
  return SkyBox(tag, T, V);
}

#if 1

// fast
float scene(Scene_DE tag, Random PRNG, Ray V)
{
  return Mandelbrot(V.origin, SkyBox(tag, Identity(), V)).x;
}

#else

// slow
float scene(Scene_DE tag, Random PRNG, Ray V)
{
  Scene_HIT HIT;
  return scene(HIT, PRNG, V).surface.de;
}

#endif

#preset LQ
Exposure = 1
TrigIter = 5
TrigLimit = 1.1
FOV = 0.8
Eye = -1.5,-2,-1
Target = -0.75,0,0
Up = 0,0,1
Steps = 100
Depth = 3
MinDist = -16
Acne = -12
Aperture = 0.01,0.01
Size = 35
Wavelengths = 300,780
Background = ../images/Berlin.equi.jpg
Distance = 100
Period = 2
MinSize = -10
Iterations = 100
Thickness = -10
#endpreset

#preset Default
FOV = 1
Eye = -1.5,-2,-1
Target = -0.75,0,0
Up = 0,0,1
Exposure = 1
TrigIter = 5
TrigLimit = 1.1
DebugDepth = false
DebugNormal = false
DebugBounce = false
Background = ../images/Berlin.equi.jpg
Distance = 100
Steps = 100
Depth = 10
MinDist = -16
Acne = -12
Aperture = 0.01,0.01
Size = 35
Wavelengths = 300,780
Period = 12
MinSize = -10
Iterations = 100
Thickness = -10
#endpreset

#preset Card
TrigIter = 5
TrigLimit = 1.10000000000000009
Exposure = 1
FOV = 1
Eye = -1.5,-2,-1
Target = -0.75,0,0
Up = 0,0,1
DebugDepth = false
DebugNormal = false
DebugBounce = false
Background = ../images/Grid.equi.png Locked
Distance = 100
Steps = 100
Depth = 3
MinDist = -16
Acne = -12
Aperture = 0.01,0.021
Size = 35
Wavelengths = 300,780
Period = 2
MinSize = -10
Iterations = 100
Thickness = -10
#endpreset
