#!/bin/sh
# image page: <https://commons.wikimedia.org/wiki/File:Reichstagsufer,_Berlin-Mitte,_360x180,_160401,_ako.jpg>
# author: Ansgar Koreng
# license: CC BY-SA 3.0 (DE) <https://creativecommons.org/licenses/by-sa/3.0/de/deed.de>
wget -c https://upload.wikimedia.org/wikipedia/commons/7/71/Reichstagsufer%2C_Berlin-Mitte%2C_360x180%2C_160401%2C_ako.jpg
cp Reichstag*.jpg Berlin.equi.jpg
