#!/bin/sh
# image page: https://commons.wikimedia.org/wiki/File:Paris_s%27%C3%A9veille_equirectangular_panorama.jpg
# author: Alexandre Duret-Lutz <https://www.flickr.com/photos/gadl/456185667/>
# license: CC-BY-SA
wget -c https://upload.wikimedia.org/wikipedia/commons/c/c6/Paris_s%27%C3%A9veille_equirectangular_panorama.jpg
cp Paris_s\'éveille_equirectangular_panorama.jpg Paris.equi.jpg
#convert Paris.equi.jpg Paris.equi.exr
#exrenvmap -c -w 2048 Paris.equi.exr Paris.cube.exr
#convert Paris.cube.exr Paris.cube.png
