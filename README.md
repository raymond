# Raymond

A physics-inspired ray tracer for Fragmentarium.

## Quick Start

Install git, install Fragmentarium.

Get the source code:

    mkdir -p ~/opt/src
    cd ~/opt/src
    git clone https://code.mathr.co.uk/raymond.git

Configure Fragmentarium: menu Edit Preferences, set Include Path to

    Examples/Include;/home/$USER/opt/src/raymond/include

replacing `$USER` with your username.

Load `~/opt/src/raymond/examples/Balls.frag` to see some shiny.

## Legal

Raymond - a physics-inspired ray tracer for Fragmentarium

Copyright (C) 2018  Claude Heiland-Allen <claude@mathr.co.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
