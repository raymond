#donotrun
#info Raymond - a physics-inspired ray tracer (C) 2018 Claude Heiland-Allen (License GPL3+)

/*
Raymond - a physics-inspired ray tracer for Fragmentarium
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

const float pi = 3.141592653589793;
const vec3 X = vec3(1.0, 0.0, 0.0);
const vec3 Y = vec3(0.0, 1.0, 0.0);
const vec3 Z = vec3(0.0, 0.0, 1.0);

#include "Complex.frag"

#include "Raymond-Hash.frag"
#include "Raymond-Defs.frag"
#ifndef RaymondNoMain
#include "Raymond-Core.frag"
#endif
#include "Raymond-Random.frag"
#include "Raymond-Halton.frag"
#include "Raymond-Quaternion.frag"
#include "Raymond-Transform.frag"
#include "Raymond-Surface.frag"
#include "Raymond-Fractal.frag"
#include "Raymond-Material.frag"
#include "Raymond-D65.frag"
#include "Raymond-Glass.frag"
#include "Raymond-Quartz.frag"
#include "Raymond-Water.frag"
#include "Raymond-Observer.frag"
#include "Raymond-FP4Plus.frag"
#include "Raymond-Screen.frag"
#include "Raymond-SkyBox.frag"
#include "Raymond-Trace.frag"
#include "Raymond-Pinhole.frag"
