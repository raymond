#donotrun

/*
Raymond - a physics-inspired ray tracer for Fragmentarium
Copyright (C) 2018  Claude Heiland-Allen
License GPL3+ <http://www.gnu.org/licenses/>
*/

#vertex

varying vec2 coord;

void main(void)
{
  gl_Position =  gl_Vertex;
  coord = ((gl_ProjectionMatrix * gl_Vertex).xy + vec2(1.0)) * 0.5;
}

#endvertex

varying vec2 coord;

uniform sampler2D frontbuffer;

#group Post

uniform float Exposure; slider[0.0,1.0,100.0]

float sRGB(float c)
{
  c = clamp(c, 0.0, 1.0);
  const float a = 0.055;
  if (c <= 0.0031308)
    return 12.92 * c;
  else
    return (1.0 + a) * pow(c, 1.0 / 2.4) - a;
}

vec3 sRGB(vec3 c)
{
  return vec3(sRGB(c.x), sRGB(c.y), sRGB(c.z));
}

void main(void)
{
  vec4 tex = texture2D(frontbuffer, coord);
  vec3 c = sRGB(Exposure * tex.xyz / tex.a);
  gl_FragColor = vec4(c, 1.0);
}
